import React, { Component } from 'react'
import './todo-list-item.css'

export default class TodoListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            done: false,
            important: false
        };
        this.onUpdateComponent = null;
    }

    onLabelClick = () => {
        this.setState(({done}) => {
            return {
                done: !done
            }
        });
    };

    onMarkImportant = () => {
        this.setState(({important}) => {
            return {
                important: !important
            }
        });
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState === this.state)
            return;

        this.onUpdateComponent(this.state);
    }

    render() {
        const { label, onDeleted, onUpdateComponent } = this.props;
        const { done, important } = this.state;

        this.onUpdateComponent = onUpdateComponent;

        let classNames = 'todo-list-item';
        if (done) {
            classNames += ' done';
        }

        if (important) {
            classNames += ' important';
        }

        return (
            <span className={classNames}>
              <span
                  className="todo-list-item-label"
                  onClick={this.onLabelClick}>
                {label}
              </span>

              <button type="button"
                      className="btn btn-outline-success btn-sm float-right"
                      onClick={this.onMarkImportant}>
                <i className="fa fa-exclamation" />
              </button>

              <button type="button"
                      className="btn btn-outline-danger btn-sm float-right"
                      onClick={onDeleted}>
                <i className="fa fa-trash-o" />
              </button>
            </span>
        );
    }
}