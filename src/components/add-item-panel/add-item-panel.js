import React from 'react'

import './add-item-panel.css'

const AddItemPanel = ({onAddItem}) => {

    return (
        <div>
            <button type="button"
                    className="btn btn-outline-secondary add-item-btn"
                    onClick={() => onAddItem('123123')}>
                Добавить элемент
            </button>
        </div>
    );

};

export default AddItemPanel;