import React, {Component} from 'react'
import AppHeader from "../app-header";
import SearchPanel from "../search-panel";
import TodoList from "../todo-list";
import ItemStatusFilter from "../item-status-filter";
import AddItemPanel from "../add-item-panel";

import './app.css'

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            todoData : [
                { id : 1, label: 'Drink Coffee' },
                { id : 2, label: 'Make awesome app' },
                { id : 3, label: 'Have drink' }
            ]
        };
    };

    addItem = (text) => {
        this.setState(({todoData}) => {
            let lastId = 1;
            if (todoData.length !== 0)
                lastId = todoData[todoData.length - 1].id + 1;
            return {
                todoData: [...todoData, {id: lastId, label: text}]
            }
        });
    };

    updateItem = (id, uiState) => {
        this.setState(({todoData}) => {

            //item in todoData before: {id: 3, label: "Have drink"}

            const objectById = todoData.filter((item) => item.id === id)[0];
            const objectWithUiState = Object.assign(objectById, uiState);

            //create copy data
            const shadowTodoData = [...todoData];
            //get element with target id index
            const indexFromId = shadowTodoData.findIndex((item) => item.id === id);
            //set updated object to same place
            shadowTodoData[indexFromId] = objectWithUiState;

            //item in todoData(shadowTodoData) after: {id: 3, label: "Have drink", done: true, important: false}

            return {
               todoData: shadowTodoData
            };
        });
    };

    deleteItem = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);

            const newArray = [
                ...todoData.slice(0, idx),
                ...todoData.slice(idx + 1)
            ];

            return {
                todoData : newArray
            }
        });
    };


    render(){

        const {todoData} = this.state;

        const inProgressCount = todoData
            .filter((item) => item.done !== true).length;

        const isDoneCount = todoData
            .filter((item) => item.done === true).length;

        return (
            <div className="todo-app">
                <AppHeader inprogress={inProgressCount} done={isDoneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel />
                    <ItemStatusFilter />
                </div>
                <TodoList
                    todos = {todoData}
                    onUpdateComponent={ this.updateItem }
                    onDeleted={ this.deleteItem }
                />
                <AddItemPanel onAddItem={ this.addItem }/>
            </div>
        );
    };
};