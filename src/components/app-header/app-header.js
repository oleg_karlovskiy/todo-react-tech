import React from 'react'
import './app-header.css';

const AppHeader = ({inprogress, done}) => {
    return (
        <div>
            <div className="app-header d-flex">
                <h1>Todo List</h1>
                <h2>{inprogress} more to do, {done} done</h2>
            </div>
        </div>
    );
};
export default AppHeader;