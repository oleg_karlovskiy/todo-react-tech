import React from 'react'
import TodoListItem from "../todo-list-item";
import './todo-list.css'

const TodoList = ({todos, onDeleted, onUpdateComponent}) => {

    const elements = todos.map((item) => {

        const {id, ...itemProps} = item;

        return (
            <li key={id} className="list-group-item ">
                <TodoListItem
                    {... itemProps}
                    onDeleted={() => onDeleted(id)}
                    onUpdateComponent={(state) => onUpdateComponent(id, state)}/>
            </li>
        );
    });

    return (
        <ul className="list-group todo-list">
            {elements}
        </ul>
    );
};
export default TodoList;